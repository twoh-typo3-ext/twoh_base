## TWOH Base
This extension allows you to move the TYPO3 context into separate files instead of working with the `AdditionalConfiguration.php`.

### Minimum requirements
* **PHP** 8
* **composer** ^2
* **TYPO3** 12

### What does it?
* Extension containing all needed files for setup TYPO3 project.
* In addition, this extension overwrites the templates and controllers from we_cookie_consent
* Includes .env support for TYPO3 AdditionalConfiguration.php

### Setup
* install Extension via Composer or FTP
* include Extension in TypoScript **ROOT Template**